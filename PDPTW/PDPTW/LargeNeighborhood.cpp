#include "LargeNeighborhood.h";
#include <list>;
#include "Commission.h";
#include <stdlib.h>  ;   /* srand, rand */
#include <time.h>;
#include "BestPlacesForCommission.h";
#include <math.h>
#include <cstdlib>;
#include <cstdio>;


void copyListToClassicArray(Commission** table, std::list<Commission*>* list)
{
	std::list<Commission*>::const_iterator iterator;
	int i = 0;

	for (iterator = list->begin(); iterator != list->end(); ++iterator) 
	{
		table[i++] = (*iterator);
	}
}

void swap(Commission** table, int i, int j)
{
	Commission *c = table[i];
	table[i] = table[j];
	table[j] = c;
}

bool accept(double v, double vprim, double temp)
{
	if (vprim < v)
	{
		return true;
	}
	else if (v == vprim)
	{
		return false;
	}
	else
	{
		double randValue = ((double)rand() / (RAND_MAX));
		double argument = (vprim - v) / temp;
		double e = exp(-1.0 * argument);
		return randValue < e;
	}
}

LargeNeighborhood::LargeNeighborhood()
{
}

Solution* LargeNeighborhood::GenerateSolution(Solution *solution, ProblemInstance *instance)
{
	srand(time(NULL));
	std::list<Commission*>* list = instance->GetCommissions();
	int size = list->size();
	Commission** table = new Commission*[size];
	copyListToClassicArray(table, list);

	Solution *bestSolutionEverSeen = solution;
	Solution *currentSolution = solution;
	double temperature = 10000.00;
	const double temepratureDecreaseRation = 0.99;
	const int maxNumberOfIterationsWithoutNewSolution = 100;
	int iterationsWithoutNewSolution = 0;
	const double ratioToRemove = 1.0 / 3.0;
	int howManyToRemove = floor(ratioToRemove * size);

	while (iterationsWithoutNewSolution != maxNumberOfIterationsWithoutNewSolution)
	{
		Solution *solutionUnderConstruction = currentSolution->GetCopy();
		std::list<Commission*> removedCommission = std::list<Commission*>();

		for (int i = 0; i < howManyToRemove; i++)
		{
			int randIndex = rand() % (size - i);
			swap(table, randIndex, size - i - 1); 
			removedCommission.push_back(table[size - i - 1]);
			solutionUnderConstruction->RemoveCommission(table[size - i - 1]);
		}

		bool breakFlag = false;
		while (!removedCommission.empty() && !breakFlag)
		{
			BestPlacesForCommission *bestInsertion = NULL;

			std::list<Commission*>::const_iterator iterator;
			for (iterator = removedCommission.begin(); iterator != removedCommission.end(); ++iterator)
			{
				BestPlacesForCommission *candiate = solutionUnderConstruction->FindBestPlace(*iterator);

				if (bestInsertion == NULL)
				{
					bestInsertion = candiate;
				}
				else if (candiate->IsBetterThan(bestInsertion))
				{
					delete bestInsertion;
					bestInsertion = candiate;
				}
				else
				{
					delete candiate;
				}
			}

			if (!bestInsertion -> HasAnyInsertion())
			{
				breakFlag = true;
			}
			else
			{
				Truck *truck = bestInsertion->GetTruck();
				Commission *commission = bestInsertion->GetCommission();
				truck->AddCommissionBestPlace(commission);
				removedCommission.remove(commission);
			}
			delete bestInsertion;
		}

		solutionUnderConstruction->RemoveEmptyTrucks();

		if (breakFlag)
		{
			delete solutionUnderConstruction;
			iterationsWithoutNewSolution++;
		}
		else if (accept(currentSolution->GetTotalDistance(), solutionUnderConstruction->GetTotalDistance(), temperature))
		{
			if (solutionUnderConstruction -> IsSolutionBetter(bestSolutionEverSeen))
			{
				if (currentSolution != bestSolutionEverSeen)
				{
					delete currentSolution;
					delete bestSolutionEverSeen;
				}
				else
				{
					delete bestSolutionEverSeen;
				}
				currentSolution = solutionUnderConstruction;
				bestSolutionEverSeen = solutionUnderConstruction;
			}
			else
			{
				if (currentSolution != bestSolutionEverSeen)
				{
					delete currentSolution;
				}
				currentSolution = solutionUnderConstruction;
			}
			iterationsWithoutNewSolution = 0;
		}
		else
		{
			delete solutionUnderConstruction;
			iterationsWithoutNewSolution++;
		}


		temperature = temperature * temepratureDecreaseRation;
	}

	if (currentSolution != bestSolutionEverSeen)
	{
		delete currentSolution;
	}

	return bestSolutionEverSeen;
}



