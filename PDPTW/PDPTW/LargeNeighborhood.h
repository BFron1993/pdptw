#pragma once
#include "Solution.h"
#include "ProblemInstance.h"

class LargeNeighborhood
{
public:
	LargeNeighborhood();
	Solution* GenerateSolution(Solution *solution, ProblemInstance *instance);
};

