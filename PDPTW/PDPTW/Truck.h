#pragma once
#include "ActionPlace.h"
#include "MyList.h";
#include "MyNode.h";
#include "Depot.h";
#include "Commission.h";
#include <iostream>
#include <fstream>

class Truck
{
private:
	MyList<ActionPlace> *schedule;
	Depot *depot;
	int maxCapacity;
	bool IsScheduleFeasible();
public:
	Truck(Depot *depot, int maxCapacity);
	~Truck();
	double GetTotalDistance();
	double FindBestPlaceForCommission(Commission *commission);
	void AddCommissionBestPlace(Commission *commission);
	void PrintStateToFile(std::ofstream *outfile);
	Truck* GetCopy();
	void RemoveCommission(Commission *commission);
	bool Empty();
};

