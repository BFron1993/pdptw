#include <iostream>;
#include "Location.h";
#include "Depot.h";
#include "ProblemInstance.h";
#include "ProblemFileReader.h";
#include <fstream>;
#include "Solution.h";
#include "SolmonsInsertion.h";
#include <conio.h>;
#include "LargeNeighborhood.h";

using namespace std;

int main()
{
	ProblemFileReader reader = ProblemFileReader();
	string fileName = "C:\\Users\\BARTEK\\Desktop\\pdptw\\pdp_100\\lc101.txt";

	ProblemInstance *a = reader.ReadProblem(fileName);

	SolmonsInsertion *solmonInsertion = new SolmonsInsertion();
	LargeNeighborhood *largeNeighborhood = new LargeNeighborhood();

	Solution *solution = solmonInsertion->PrepareInitialSolution(a);
	solution = largeNeighborhood->GenerateSolution(solution, a);

	cout << solution->GetTotalDistance() << endl;
	cout << solution->GetNumberOfHolons() << endl;
	solution->PrintSolutionToFile();
	getch();
}