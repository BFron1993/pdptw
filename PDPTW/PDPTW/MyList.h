#pragma once
#include "MyNode.h"
#include <stdio.h>;

template < class T >

class MyList
{
private:
	MyNode<T> *head;
	MyNode<T> *tail;

public:
	MyNode<T>* GetHead()
	{
		return head;
	}

	MyNode<T>* GetTail()
	{
		return tail;
	}

	MyList<T>()
	{
		head = NULL;
		tail = NULL;
	}

	void Remove(T *data)
	{
		if (head == NULL)
		{
			return;
		}
		else
		{
			MyNode<T> *pointer = head;
			do
			{
				if (pointer -> GetData() == data)
				{
					MyNode<T> *prev = pointer->prev;
					MyNode<T> *next = pointer->next;

					if (prev == NULL)
					{
						head = next;
					}
					else
					{
						prev->next = next;
					}

					if (next == NULL)
					{
						tail = prev;
					}
					else
					{
						next->prev = prev;
					}

					delete pointer;

					return;
				}
				pointer = pointer->next;
			} while (pointer != NULL);

		}
	}

	MyNode<T>* AddDataFront(T *data)
	{
		MyNode<T> *newNode = newNode<T>(data);

		if (head == NULL)
		{
			head = newNode;
			tail = newNode;
		}
		else
		{
			*newNode -> next = head;
			head->prev = newNode
			head = newNode;
		}

		return newNode;
	}

	MyNode<T>* AddDataBack(T *data)
	{
		MyNode<T> *newNode = new MyNode<T>(data);

		if (tail == NULL)
		{
			head = newNode;
			tail = newNode;
		}
		else
		{
			newNode->prev = tail;
			tail->next = newNode;
			tail = newNode;
		}

		return newNode;
	}

	MyNode<T>* AddAfterElement(MyNode<T> *element, T *data)
	{
		MyNode<T> *newNode = new MyNode<T>(data);

		if (element -> next == NULL)
		{
			element->next = newNode;
			newNode->prev = element;
			tail = newNode;
		}
		else
		{
			MyNode<T> *nextElement = element->next;
			newNode->prev = element;
			newNode->next = nextElement;

			element->next = newNode;
			nextElement->prev = newNode;		
		}

		return newNode;
	}

	MyNode<T>* AddBeforeElement(MyNode<T> *element, T *data)
	{
		MyNode<T> *newNode = new MyNode<T>(data);
		 
		if (element->prev == NULL)
		{
			element->prev = newNode;
			newNode->next = element;
			head = newNode;
		}
		else
		{
			MyNode<T> *prevElement = element->prev;
			newNode->next = element;
			newNode->prev = prevElement;
			prevElement->next = newNode;
			element->prev = newNode;
		}

		return newNode;
	}

	MyList<T>* GetCopy()
	{
		MyList<T> *newList = new MyList<T>();

		MyNode<T> *node = head;

		while (node != NULL)
		{
			newList->AddDataBack(node->GetData());
			node = node->next;
		}

		return newList;
	}

	~MyList<T>()
	{
		MyNode<T> *toDelete;
		MyNode<T> *node = head;

		while (node != NULL)
		{
			toDelete = node;
			node = node->next;
			delete toDelete;
		}
	}
};