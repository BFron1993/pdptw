#pragma once
#include <list>
#include "Commission.h"
#include "Depot.h"

class ProblemInstance
{
private:
	std::list<Commission*> *commisions;
	Depot *depot;
	int maxCapacity;

public:
	ProblemInstance(int maxCapacity, Depot *depot);
	void AddCommission(Commission *commission);
	std::list<Commission*>* GetCommissions();
	Depot* GetDepot();
	int GetMaxCapacity();
};

