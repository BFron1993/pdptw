#include "BestPlacesForCommission.h"

void BestPlacesForCommission::init()
{
	this->bestCandidateToInsert = NULL;
	this->bestInsertionCost = std::numeric_limits<double>::max();
	this->secondBestInsertionCost = std::numeric_limits<double>::max();
}

void BestPlacesForCommission::init(Commission *commission)
{
	this->commission = commission;
}

void BestPlacesForCommission::init(Commission *commission, Truck *bestCandidateToInsert, double bestInsertionCost)
{
	this->bestCandidateToInsert = bestCandidateToInsert;
	this->bestInsertionCost = bestInsertionCost;
	init(commission);
}


void BestPlacesForCommission::init(Commission *commission, Truck *bestCandidateToInsert, double bestInsertionCost, double secondBestInsertionCost)
{
	this->secondBestInsertionCost = secondBestInsertionCost;
	init(commission, bestCandidateToInsert, bestInsertionCost);
}

BestPlacesForCommission::BestPlacesForCommission(Commission *commission)
{
	init();
	init(commission);
}


BestPlacesForCommission::BestPlacesForCommission(Commission *commission, Truck *bestCandidateToInsert, double bestInsertionCost)
{
	init();
	init(commission, bestCandidateToInsert, bestInsertionCost);
}


BestPlacesForCommission::BestPlacesForCommission(Commission *commission, Truck *bestCandidateToInsert, double bestInsertionCost, double secondBestInsertionCost)
{
	init();
	init(commission, bestCandidateToInsert, bestInsertionCost, secondBestInsertionCost);
}

Commission* BestPlacesForCommission::GetCommission()
{
	return this->commission;
}

Truck* BestPlacesForCommission::GetTruck()
{
	return this->bestCandidateToInsert;
}

bool BestPlacesForCommission::HasAnyInsertion()
{
	return this->bestCandidateToInsert != NULL;
}

bool BestPlacesForCommission::HasExactlyOneInsertion()
{
	return this->secondBestInsertionCost == std::numeric_limits<double>::max() && this->secondBestInsertionCost != std::numeric_limits<double>::max();
}

bool BestPlacesForCommission::IsBetterThan(BestPlacesForCommission *bestPlacesForAnotherCommission)
{
	if (this->bestCandidateToInsert == NULL)
	{
		return true;
	}

	double myRegret = this->secondBestInsertionCost - this->bestInsertionCost;
	double otherRegret = bestPlacesForAnotherCommission->secondBestInsertionCost - this->bestInsertionCost;
	return myRegret > otherRegret;
}
