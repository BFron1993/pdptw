#pragma once
#include <stdio.h>;

template <class T>

class MyNode
{
private:
	T *data;
public:
	MyNode<T> *prev;
	MyNode<T> *next;

	T* GetData()
	{
		return data;
	}

	MyNode<T>(T* data)
	{
		prev = NULL;
		next = NULL;
		this->data = data;
	}
};