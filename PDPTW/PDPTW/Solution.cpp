#include "Solution.h"

Solution::Solution()
{
	trucks = new std::list<Truck*>();
}

void Solution::AddTruck(Truck *truck)
{
	trucks->push_back(truck);
}

double Solution::GetTotalDistance()
{
	std::list<Truck*>::const_iterator iterator;
	double distance = 0.0;

	for (iterator = this->trucks->begin(); iterator != this->trucks->end(); ++iterator) {
		distance += (*iterator)->GetTotalDistance();
	}

	return distance;
}

int Solution::GetNumberOfHolons()
{
	return this->trucks->size();
}

void Solution::PrintSolutionToFile()
{
	std::ofstream outfile = std::ofstream("out.txt", std::fstream::trunc | std::fstream::out);
	int i = 1;
	std::list<Truck*>::const_iterator iterator;

	for (iterator = this->trucks->begin(); iterator != this->trucks->end(); ++iterator) {
		outfile <<"Route";
		outfile <<(i);
		outfile << ": ";
		(*iterator)->PrintStateToFile(&outfile);
		i++;
		outfile << std::endl;
	}

	outfile.close();
}

Solution* Solution::GetCopy()
{
	Solution *newSolution = new Solution();

	std::list<Truck*>::const_iterator iterator;

	for (iterator = this->trucks->begin(); iterator != this->trucks->end(); ++iterator) {
		newSolution->AddTruck((*iterator)->GetCopy());
	}

	return newSolution;
}

void Solution::RemoveCommission(Commission *commission)
{
	std::list<Truck*>::const_iterator iterator;

	for (iterator = this->trucks->begin(); iterator != this->trucks->end(); ++iterator) {
		(*iterator)->RemoveCommission(commission);
	}
}

BestPlacesForCommission* Solution::FindBestPlace(Commission *commission)
{
	Truck *bestTruckToInsert = NULL;
	double bestCost = std::numeric_limits<double>::max();
	double secodnBestConstToInsert = std::numeric_limits<double>::max();

	std::list<Truck*>::const_iterator iterator;

	for (iterator = this->trucks->begin(); iterator != this->trucks->end(); ++iterator) {
		double result = (*iterator)->FindBestPlaceForCommission(commission);
		if (result != -1.0)
		{
			if (result < bestCost)
			{
				secodnBestConstToInsert = bestCost;
				bestCost = result;
				bestTruckToInsert = (*iterator);
			}
			else if (result < secodnBestConstToInsert)
			{
				secodnBestConstToInsert = result;
			}
		}
	}

	if (bestTruckToInsert == NULL)
	{
		return new BestPlacesForCommission(commission);
	}
	else
	{
		if (secodnBestConstToInsert == std::numeric_limits<double>::max())
		{
			return new BestPlacesForCommission(commission, bestTruckToInsert, bestCost);
		}
		else
		{
			return new BestPlacesForCommission(commission, bestTruckToInsert, bestCost, secodnBestConstToInsert);
		}
	}
}

Solution::~Solution()
{
	std::list<Truck*>::const_iterator iterator;

	for (iterator = this->trucks->begin(); iterator != this->trucks->end(); ++iterator) {
		Truck* truck = (*iterator);
		delete truck;
	}

	delete this->trucks;
}

bool Solution::IsSolutionBetter(Solution *anotherSolution)
{
	if (this->GetNumberOfHolons() == anotherSolution -> GetNumberOfHolons())
	{
		return this->GetTotalDistance() < anotherSolution->GetTotalDistance();
	}
	else
	{
		return this->GetNumberOfHolons() < anotherSolution->GetNumberOfHolons();
	}
}

void Solution::RemoveEmptyTrucks()
{
	std::list<Truck*>::const_iterator iterator;

	Truck* candiateToRemove = NULL;

	for (iterator = this->trucks->begin(); iterator != this->trucks->end(); ++iterator) {
		if ((*iterator)-> Empty())
		{
			candiateToRemove = (*iterator);
		}
	}

	if (candiateToRemove != NULL)
	{
		trucks->remove(candiateToRemove);
		delete candiateToRemove;
		RemoveEmptyTrucks();
	}
}
