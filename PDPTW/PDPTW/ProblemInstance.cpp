#include "ProblemInstance.h"
#include <list>
#include "Commission.h"


ProblemInstance::ProblemInstance(int maxCapacity, Depot *depot)
{
	this->maxCapacity = maxCapacity;
	this->depot = depot;
	this->commisions = new std::list<Commission*>();
}

void ProblemInstance::AddCommission(Commission *commission)
{
	this->commisions->push_back(commission);
}

std::list<Commission*>* ProblemInstance::GetCommissions()
{
	std::list<Commission*>::const_iterator iterator;
	std::list<Commission*> *listToRet = new std::list<Commission*>();

	for (iterator = this->commisions->begin(); iterator != this->commisions->end(); ++iterator) {
		listToRet->push_back(*iterator);
	}

	return listToRet;
}

Depot* ProblemInstance::GetDepot()
{
	return this->depot;
}

int ProblemInstance::GetMaxCapacity()
{
	return maxCapacity;
}
