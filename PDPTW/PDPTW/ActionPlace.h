#pragma once
#include "Location.h"
class ActionPlace : public Location
{
private:
	int id;
	double timeWindowStart;
	double timeWindowClose;
	int demand;
	double serviceTime;

public:
	ActionPlace(int x, int y, int id, int timeWindowStart, int timeWindowClose, int demand, int serviceTime);
	int GetId();
	double GetTimeWindowStart();
	double GetTimeWindowClose();
	int GetDemand();
	double GetServiceTime();
};

