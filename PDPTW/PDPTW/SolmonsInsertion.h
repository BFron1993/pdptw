#pragma once
#include "Solution.h"
#include "ProblemInstance.h"

class SolmonsInsertion
{
public:
	SolmonsInsertion();
	Solution* PrepareInitialSolution(ProblemInstance *problem);
};

