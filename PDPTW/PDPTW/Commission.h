#pragma once
#include "ActionPlace.h"
#include "Depot.h"
class Commission
{
private:
	ActionPlace *pickup;
	ActionPlace *delivery;
	double distanceToDepot;

public:
	Commission(ActionPlace *pickup, ActionPlace *delivery);
	double CountDistanceToDepot(Depot *depot);
	ActionPlace* GetPickup();
	ActionPlace* GetDelivery();
};

