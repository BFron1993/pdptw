#pragma once
class Location
{
private:
	int x;
	int y;

public:
	Location(int x, int y);
	double DistanceToLocation(Location *location);
};

