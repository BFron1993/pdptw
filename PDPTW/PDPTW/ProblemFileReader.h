#pragma once
#include <string>
#include "ProblemInstance.h"

class ProblemFileReader
{
public:
	ProblemFileReader();
	ProblemInstance* ReadProblem(std::string filePath);
};

