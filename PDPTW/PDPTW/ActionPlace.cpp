#include "ActionPlace.h"

ActionPlace::ActionPlace(int x, int y, int id, int timeWindowStart, int timeWindowClose, int demand, int serviceTime) : Location(x, y)
{
	this->id = id;
	this->timeWindowStart = (double)timeWindowStart;
	this->timeWindowClose = (double)timeWindowClose;
	this->demand = demand;
	this->serviceTime = (double)serviceTime;
}

int ActionPlace::GetId()
{
	return this->id;
}

int ActionPlace::GetDemand()
{
	return this->demand;
}

double ActionPlace::GetTimeWindowStart()
{
	return this->timeWindowStart;
}

double ActionPlace::GetTimeWindowClose()
{
	return this->timeWindowClose;
}

double ActionPlace::GetServiceTime()
{
	return this->serviceTime;
}