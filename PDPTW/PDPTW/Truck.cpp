#include "Truck.h";
#include <stdio.h>;
#include <limits>

Truck::Truck(Depot *depot, int maxCapacity)
{
	this->depot = depot;
	schedule = new MyList<ActionPlace>();
	this->maxCapacity = maxCapacity;
}

double Truck::GetTotalDistance()
{
	double distance = 0.0;

	Location *prevLocation = this->depot;

	MyNode<ActionPlace> *node = schedule->GetHead();

	while (node != NULL)
	{
		distance += prevLocation->DistanceToLocation(node ->GetData());
		prevLocation = node->GetData();
		node = node->next;
	}

	distance += prevLocation->DistanceToLocation(depot);
	return distance;
}

double Truck::FindBestPlaceForCommission(Commission *commission)
{
	double distanceBefore = GetTotalDistance();
	double newBestDistance = std::numeric_limits<double>::max();

	MyNode<ActionPlace> *nodeToInsertPickupBefore = schedule->GetHead();

	while (nodeToInsertPickupBefore != NULL)
	{
		MyNode<ActionPlace> *nodeToInsertDeliveryAfter = schedule->AddBeforeElement(nodeToInsertPickupBefore, commission->GetPickup());

		while (nodeToInsertDeliveryAfter != NULL)
		{
			schedule->AddAfterElement(nodeToInsertDeliveryAfter, commission->GetDelivery());

			if (IsScheduleFeasible())
			{
				double currentDistance = GetTotalDistance();
				if (currentDistance < newBestDistance)
				{
					newBestDistance = currentDistance;
				}
			}

			schedule->Remove(commission->GetDelivery());

			nodeToInsertDeliveryAfter = nodeToInsertDeliveryAfter->next;
		}

		schedule->Remove(commission->GetPickup());
		nodeToInsertPickupBefore = nodeToInsertPickupBefore->next;
	}

	schedule->AddDataBack(commission->GetPickup());
	schedule->AddDataBack(commission->GetDelivery());

	if (IsScheduleFeasible())
	{
		double currentDistance = GetTotalDistance();
		if (currentDistance < newBestDistance)
		{
			newBestDistance = currentDistance;
		}
	}

	schedule->Remove(commission->GetPickup());
	schedule->Remove(commission->GetDelivery());

	if (newBestDistance == std::numeric_limits<double>::max())
	{
		return -1.0;
	}
	else
	{
		return newBestDistance - distanceBefore;
	}
}

void Truck::AddCommissionBestPlace(Commission *commission)
{
	double distanceBefore = GetTotalDistance();
	double newBestDistance = std::numeric_limits<double>::max();

	MyNode<ActionPlace> * bestNodeToInsertPickupBefore;
	MyNode<ActionPlace> * bestNodeToInsertDeliveryBefore;

	MyNode<ActionPlace> *firstIterator = schedule->GetHead();

	while (firstIterator != NULL)
	{

		MyNode<ActionPlace> *secondIterator = schedule->AddBeforeElement(firstIterator, commission->GetPickup());
		MyNode<ActionPlace> *candidateToInsertPickupBefore = firstIterator;

		while (secondIterator != NULL)
		{
			MyNode<ActionPlace> *candidateToInsertDeliveryBefore = (schedule->AddAfterElement(secondIterator, commission->GetDelivery()))->next;

			if (IsScheduleFeasible())
			{
				double currentDistance = GetTotalDistance();
				if (currentDistance < newBestDistance)
				{
					newBestDistance = currentDistance;
					bestNodeToInsertPickupBefore = candidateToInsertPickupBefore;
					bestNodeToInsertDeliveryBefore = candidateToInsertDeliveryBefore;
				}
			}

			schedule->Remove(commission->GetDelivery());

			secondIterator = secondIterator->next;
		}

		schedule->Remove(commission->GetPickup());
		firstIterator = firstIterator->next;
	}

	schedule->AddDataBack(commission->GetPickup());
	schedule->AddDataBack(commission->GetDelivery());

	if (IsScheduleFeasible())
	{
		double currentDistance = GetTotalDistance();
		if (currentDistance < newBestDistance)
		{
			return;
		}
	}

	schedule->Remove(commission->GetPickup());
	schedule->Remove(commission->GetDelivery());

	if (newBestDistance == std::numeric_limits<double>::max())
	{
		throw "CouldNotFindFeasibleInsertion!";
	}
	else
	{
		schedule->AddBeforeElement(bestNodeToInsertPickupBefore, commission->GetPickup());
		if (bestNodeToInsertDeliveryBefore != NULL)
		{
			schedule->AddBeforeElement(bestNodeToInsertDeliveryBefore, commission->GetDelivery());
		}
		else
		{
			schedule->AddDataBack(commission->GetDelivery());
		}
	}
}

bool Truck::IsScheduleFeasible()
{
	int currentCapacity = 0;
	double currentTime = 0.0;
	Location *prevLocation = depot;

	MyNode<ActionPlace> *iterator = schedule->GetHead();

	while (iterator != NULL)
	{
		ActionPlace *currentAction = iterator->GetData();
		currentTime += prevLocation->DistanceToLocation(currentAction);

		if (currentTime > currentAction->GetTimeWindowClose())
		{
			return false;
		}
		else if (currentTime < currentAction->GetTimeWindowStart())
		{
			currentTime = currentAction->GetTimeWindowStart();
		}

		currentCapacity += currentAction->GetDemand();

		if (currentCapacity > maxCapacity)
		{
			return false;
		}

		currentTime += currentAction->GetServiceTime();

		prevLocation = currentAction;
		iterator = iterator->next;
	}
	return true;
}

void Truck::PrintStateToFile(std::ofstream *outfile)
{
	MyNode<ActionPlace> *iterator = schedule->GetHead();

	while (iterator != NULL)
	{
		ActionPlace *a =iterator->GetData();

		int b = a->GetId();

		outfile ->operator<<(b) << " ";
		
		iterator = iterator->next;
	}
};

Truck* Truck::GetCopy()
{
	Truck* newTruck = new Truck(this->depot, this->maxCapacity);
	newTruck->schedule = this->schedule->GetCopy();
	
	return newTruck;
}

void Truck::RemoveCommission(Commission *commission)
{
	this->schedule->Remove(commission->GetPickup());
	this->schedule->Remove(commission->GetDelivery());
}

Truck::~Truck()
{
	delete this->schedule;
}

bool Truck::Empty()
{
	return this->schedule->GetHead() == NULL;
}