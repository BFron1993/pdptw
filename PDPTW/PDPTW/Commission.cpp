#include "Commission.h"
#include "ActionPlace.h"

Commission::Commission(ActionPlace *pickup, ActionPlace *delivery)
{
	this->pickup = pickup;
	this->delivery = delivery;
	this->distanceToDepot = -1;
}

double Commission::CountDistanceToDepot(Depot *depot)
{
	if (this->distanceToDepot == -1)
	{
		double d1 = depot->DistanceToLocation(this->pickup);
		double d2 = this->pickup->DistanceToLocation(this->delivery);
		double d3 = this->delivery->DistanceToLocation(depot);

		this->distanceToDepot = d1 + d2 + d3;
	}

	return this->distanceToDepot;
}

ActionPlace* Commission::GetPickup()
{
	return this->pickup;
}

ActionPlace* Commission::GetDelivery()
{
	return this->delivery;
}
