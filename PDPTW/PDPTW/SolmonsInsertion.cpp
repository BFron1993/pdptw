#include "SolmonsInsertion.h"
#include "Solution.h"
#include <limits>


SolmonsInsertion::SolmonsInsertion()
{
}

Solution* SolmonsInsertion::PrepareInitialSolution(ProblemInstance *probleminstance)
{
	Depot *depot = probleminstance->GetDepot();
	std::list<Commission*> *unroutedCommissions = probleminstance->GetCommissions();
	int maxCapacity = probleminstance->GetMaxCapacity();

	Solution * solution = new Solution();

	while (!unroutedCommissions -> empty())
	{
		double maxOneCommissionCost = 0.0;
		Commission *hardestCommission;

		std::list<Commission*>::const_iterator iterator;

		for (iterator = unroutedCommissions->begin(); iterator != unroutedCommissions->end(); ++iterator) {

			double commissionDistance = (*iterator)->CountDistanceToDepot(depot);
			if (commissionDistance > maxOneCommissionCost)
			{
				maxOneCommissionCost = commissionDistance;
				hardestCommission = (*iterator);
			}
		}

		Truck *truck = new Truck(depot, maxCapacity);
		truck->AddCommissionBestPlace(hardestCommission);
		unroutedCommissions->remove(hardestCommission);

		do
		{
			double maxIncome = std::numeric_limits<double>::max();
			Commission *easiestCommission;

			for (iterator = unroutedCommissions->begin(); iterator != unroutedCommissions->end(); ++iterator) {

				double distanceIncome = truck->FindBestPlaceForCommission((*iterator));

				if (distanceIncome != -1.0)
				{
					if (distanceIncome < maxIncome)
					{
						maxIncome = distanceIncome;
						easiestCommission = (*iterator);
					}
				}
			}

			if (maxIncome == std::numeric_limits<double>::max())
			{
				solution->AddTruck(truck);
				break;
			}
			else
			{
				truck->AddCommissionBestPlace(easiestCommission);
				unroutedCommissions->remove(easiestCommission);
				if (unroutedCommissions->empty())
				{
					solution->AddTruck(truck);
				}
			}

		} while (!unroutedCommissions->empty());
	}

	return solution;
}