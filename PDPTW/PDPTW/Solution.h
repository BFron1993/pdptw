#pragma once
#include <list>
#include "Truck.h"
#include <iostream>
#include <fstream>
#include <string>
#include "BestPlacesForCommission.h";

class Solution
{
private:
	std::list<Truck*> *trucks;

public:
	Solution();
	~Solution();
	void AddTruck(Truck *truck);
	double GetTotalDistance();
	int GetNumberOfHolons();
	void PrintSolutionToFile();
	Solution* GetCopy();
	void RemoveCommission(Commission *commission);
	BestPlacesForCommission* FindBestPlace(Commission *commission);
	bool IsSolutionBetter(Solution *anotherSolution);
	void RemoveEmptyTrucks();
};

