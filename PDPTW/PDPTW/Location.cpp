#include "Location.h"
#include "math.h"

Location::Location(int x, int y)
{
	this->x = x;
	this->y = y;
}

double Location::DistanceToLocation(Location *location)
{
	int xDifference = abs(location->x - this->x);
	int yDifference = abs(location->y - this->y);

	return sqrt((double)((xDifference * xDifference) + (yDifference * yDifference)));
}
