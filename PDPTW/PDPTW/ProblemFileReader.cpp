#include "ProblemFileReader.h"
#include <fstream>
#include <iostream>
#include <vector>
#include "Depot.h"
#include <map>
#include "ActionPlace.h"

using namespace std;

vector<string> split(const char *str, char c = '\t')
{
	vector<string> result;

	do
	{
		const char *begin = str;

		while (*str != c && *str)
			str++;

		result.push_back(string(begin, str));
	} while (0 != *str++);

	return result;
}

ProblemFileReader::ProblemFileReader(){}

ProblemInstance* ProblemFileReader::ReadProblem(std::string filePath)
{
	string line;
	ifstream infile(filePath);
	vector<string> tokens;
	ProblemInstance *instance;
	int id;
	int x;
	int y;
	int demand;
	int serviceTime;
	int windowStart;
	int windowClose;
	int pickup;
	int delivery;

	if (infile.is_open())
	{
		getline(infile, line);
		tokens = split(line.c_str());
		int maxCapacity = atoi(tokens[1].c_str());

		getline(infile, line);
		tokens = split(line.c_str());
		x = atoi(tokens[1].c_str());
		y = atoi(tokens[2].c_str());

		Depot *depot = new Depot(x, y);
		instance = new ProblemInstance(maxCapacity, depot);

		map<int, ActionPlace*> *pickups = new map<int, ActionPlace*>();
		map<int, ActionPlace*> *deliveries = new map<int, ActionPlace*>();

		while (getline(infile, line))
		{
			tokens = split(line.c_str());

			id = atoi(tokens[0].c_str());
			x = atoi(tokens[1].c_str());
			y = atoi(tokens[2].c_str());
			demand = atoi(tokens[3].c_str());
			windowStart = atoi(tokens[4].c_str());
			windowClose = atoi(tokens[5].c_str());
			serviceTime = atoi(tokens[6].c_str());
			pickup = atoi(tokens[7].c_str());
			delivery = atoi(tokens[8].c_str());

			ActionPlace *action = new ActionPlace(x, y, id, windowStart, windowClose, demand, serviceTime);
			if (pickup != 0)
			{
				deliveries->insert(std::make_pair(id, action));
			}
			else
			{
				pickups->insert(std::make_pair(delivery, action));
			}
		}
		infile.close();

		for (std::map<int, ActionPlace*>::iterator iter = pickups -> begin(); iter != pickups -> end(); ++iter)
		{
			int k = iter->first;
			ActionPlace* vPickup = iter->second;

			ActionPlace* vDelivery = deliveries->operator[](k);

			Commission *commission = new Commission(vPickup, vDelivery);
			instance->AddCommission(commission);
		}

		delete pickups;
		delete deliveries;
		return instance;
	}
	else
	{
		cout << "File was unable to open!\n";
		return NULL;
	}

}