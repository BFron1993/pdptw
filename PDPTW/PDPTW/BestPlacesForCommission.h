#pragma once
#include "Truck.h";
#include "Commission.h";

class BestPlacesForCommission
{
private:
	Truck *bestCandidateToInsert;
	double bestInsertionCost;
	double secondBestInsertionCost;
	Commission *commission;
	void init();
	void init(Commission *commission);
	void init(Commission *commission, Truck *bestCandidateToInsert, double bestInsertionCost);
	void init(Commission *commission, Truck *bestCandidateToInsert, double bestInsertionCost, double secondBestInsertionCost);

public:
	BestPlacesForCommission(Commission *commission);
	BestPlacesForCommission(Commission *commission, Truck *bestCandidateToInsert, double bestInsertionCost);
	BestPlacesForCommission(Commission *commission, Truck *bestCandidateToInsert, double bestInsertionCost, double secondBestInsertionCost);
	Commission* GetCommission();
	Truck* GetTruck();
	bool HasAnyInsertion();
	bool HasExactlyOneInsertion();
	bool IsBetterThan(BestPlacesForCommission *bestPlacesForAnotherCommission);
};

